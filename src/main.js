import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Login from './components/login.vue'
import Register from './components/register.vue'
import store from './store'


Vue.use(VueRouter)

const routes = [
  { path: '/login', component: Login},
  { path: '/register', component: Register}
];

const router = new VueRouter  ({
  routes,
  mode: 'history'
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
