const state = {
    checklists: [
        {
          id:1,
          title: "Checklist 1",
          completed: false
        },
        {
          id:2,
          title: "Checklist 2",
          completed: false
        },   
        {
          id:3,
          title: "Checklist 3",
          completed: false
        }       
        ]
};

const getters = {
    allChecklists: state => state.checklists
};

const actions = {
    fetchChecklist({commit}, checklists) {
        commit('setChecklists', checklists)
    },
    addChecklist({commit}, checklist) {
        commit('newChecklist', checklist)
    },
    updateChecklist({commit}, checklist) {
      commit('updChecklist', checklist)
    },
    removeChecklist({commit}, checklist) {
      commit('delChecklist', checklist)
    }
};

const mutations = {
    setChecklists: ( state, checklists) => state.checklists = checklists,
    newChecklist: (state, checklist) => state.checklists.push(checklist),
    updChecklist: (state, updatedChecklist) => {
      const index = state.checklists.findIndex(c => c.id === updatedChecklist.id)
      if(index !== -1) {
        state.checklists.splice(index, 1, updatedChecklist)
      }
    },
    delChecklist: (state, checklist) => state.checklists = state.checklists.filter(c => c.id !== checklist.id)
};

export default { state, getters, actions, mutations }