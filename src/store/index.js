import Vuex from 'vuex'
import Vue from 'vue'
import checklists from './modules/checklists'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        checklists
    }
})